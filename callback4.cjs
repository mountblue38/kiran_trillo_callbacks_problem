const callback1Fun = require("./callback1.cjs");
const callback2Fun = require("./callback2.cjs");
const callback3Fun = require("./callback3.cjs");

const fs = require("fs");
const path = require("path");
const boardPath = path.join(__dirname, "./Data/boards.json");

function callback4(inputName) {
    setTimeout(() => {
        fs.readFile(boardPath, "utf-8", (err, data) => {
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                let finder = data.find((element) => {
                    return element.name == inputName;
                });

                if (finder) {
                    callback1Fun(finder.id, (err, data) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(data);

                            return callback2Fun(data.id, (err, data) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log(data);

                                    data.map((element) => {
                                        if (element.name == "Mind") {
                                            return callback3Fun(element.id, (err, data) => {
                                                if (err) {
                                                    console.log(err);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    console.log("Name not found");
                }
            }
        });
    }, 2 * 1000);
}

module.exports = callback4;
