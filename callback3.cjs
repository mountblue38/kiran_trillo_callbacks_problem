const fs = require("fs");
const path = require("path");
const cardsPath = path.join(__dirname, "./Data/cards.json");

function callback2(lstId, cbFunction) {
    setTimeout(() => {
        fs.readFile(cardsPath, "utf-8", (err, data) => {
            if (err) {
                console.error("Error in reading the file ");
                return cbFunction(err);
            } else {
                let res;
                data = JSON.parse(data);

                Object.keys(data).map((elementId) => {
                    if (elementId == lstId) {
                        return (res = data[lstId]);
                    }
                });

                if (res) {
                    return cbFunction(null, res);
                } else {
                    return cbFunction("ID Not found");
                }
            }
        });
    }, 2 * 1000);
}

module.exports = callback2;
