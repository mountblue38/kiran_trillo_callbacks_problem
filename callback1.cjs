const fs = require("fs");
const path = require("path");
const boardPath = path.join(__dirname, "./Data/boards.json");

function callback1(boardId, cbFunction) {
    setTimeout(() => {
        fs.readFile(boardPath, "utf-8", (err, data) => {
            if (err) {
                console.error("Error in reading the file ");
                return cbFunction(err);
            } else {
                data = JSON.parse(data);
                let res = data.find((element) => {
                    return element.id == boardId;
                });

                if (res) {
                    return cbFunction(null, res);
                } else {
                    return cbFunction("ID Not found");
                }
            }
        });
    }, 2 * 1000);
}

module.exports = callback1;
