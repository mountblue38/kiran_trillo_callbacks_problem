const fs = require("fs");
const path = require("path");
const listsPath = path.join(__dirname, "./Data/lists.json");

function callback2(boardId, cbFunction) {
    setTimeout(() => {
        fs.readFile(listsPath, "utf-8", (err, data) => {
            if (err) {
                console.error("Error in reading the file ");
                return cbFunction(err);
            } else {
                let res;
                data = JSON.parse(data);

                Object.keys(data).map((elementId) => {
                    if (elementId == boardId) {
                        return (res = data[boardId]);
                    }
                });

                if (res) {
                    return cbFunction(null, res);
                } else {
                    return cbFunction("ID Not found");
                }
            }
        });
    }, 2 * 1000);
}

module.exports = callback2;
